from django.apps import AppConfig


class AppmusicaConfig(AppConfig):
    name = 'AppMusica'
