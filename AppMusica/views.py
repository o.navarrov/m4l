from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'AppMusica/INDEX.html',{})

def InicioSesion(request):
    return render(request,'AppMusica/InicioSesion.html',{})

def Formulario(request):
    return render(request,'AppMusica/Formulario.html',{})
    
def Encuesta(request):
    return render(request,'AppMusica/Encuesta.html',{})
